+++
date = "2018-04-04"
title = "DUNE 2.5.2rc1 Released"
+++

The first release candidate for the upcoming 2.5.2 release is now
available.  You can [download the tarballs](/releases/2.5.2rc1) or
checkout the `v2.5.2rc1` tag via Git.

From the <span style="font-variant: small-caps">Dune</span> core
modules, only _dune-common_, _dune-grid_ and _dune-localfunctions_ had
changes from 2.5.1; from the staging modules only _dune-uggrid_ was
updated.

Please go and test, and report any problems that you encounter.

<span style="font-variant: small-caps">Dune</span> 2.5.2 is a bug fix
release to keep <span style="font-variant: small-caps">Dune</span> 2.5
working for recent systems. It is advised to move to <span
style="font-variant: small-caps">Dune</span> 2.6.
