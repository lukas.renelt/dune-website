+++
date = "2014-06-12"
title = "New UG Release"
+++

Version 3.11.0 of the [UG software](http://www.iwr.uni-heidelberg.de/frame/iwrwikiequipment/software/ug) has been released. It brings various fixes related to dynamic load balancing. This release will shortly be made the new minimum supported version for dune-grid master.
