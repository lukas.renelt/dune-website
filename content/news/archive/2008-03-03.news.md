+++
date = "2008-03-03"
title = "DUNE Developer Meeting"
+++

A meeting of all DUNE developers was held on 27. and 28. of February 2008 at the Freie Universität Berlin. The main issue was an evaluation and revision of the grid interface. A list of projected changes to the interface can be found in the [meeting minutes](/community/meetings/2008-02-27-devmeeting). We also decided to have three new core modules: dune-shapefunctions, dune-functions, and dune-grid-dev-howto. These will appear soon.
