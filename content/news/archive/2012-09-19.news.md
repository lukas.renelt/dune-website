+++
date = "2012-09-19"
title = "DUNE Binary Packages for openSuse"
+++

We are happy to announce that after Debian, now also openSuse added binary packages of DUNE to their repositories.

**openSuse** includes DUNE in the newest release `12.2`. `RPM`s for the DUNE core modules and `dune-PDELab` are available from the [science repository](http://download.opensuse.org/repositories/science/openSUSE_12.2/x86_64/).

For your convenience, packages for `Alberta` and `ALUGrid` are also included.
