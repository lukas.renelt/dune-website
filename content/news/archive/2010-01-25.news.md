+++
date = "2010-01-25"
title = "New ALUGrid Version"
+++

ALUGrid version 1.22 has been released. Generating ALUGrid's no longer needs temporary macro grid files. Also, some bug fixes have been done. The trunk of Dune-Grid and also the upcoming release 2.0 of Dune-Grid will only work with this or newer versions. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
