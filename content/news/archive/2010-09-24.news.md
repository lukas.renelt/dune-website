+++
date = "2010-09-24"
title = "New ALUGrid Version"
+++

ALUGrid version 1.23 has been released. This version contains some bug fixes, for example the one with the incorrect alugrid.pc file. Furthermore, this version enables periodic boundaries in 3d when using the trunk of dune-grid. The new version is available from the [ALUGrid page](http://www.mathematik.uni-freiburg.de/IAM/Research/alugrid/).
