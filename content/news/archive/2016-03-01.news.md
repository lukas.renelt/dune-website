+++
date = "2016-03-01"
title = "DUNE is part of GSoC 2016"
+++


![](/img/gsoc/GSoC2016Logo.png)
 Google Summer of Code 2016
 [![Creative Commons License](http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png)](http://creativecommons.org/licenses/by-nc-nd/3.0/)

We are happy to let you know, that DUNE has been accepted as a mentoring organization in [Google Summer of Code 2016](https://summerofcode.withgoogle.com/organizations/5666115405479936/). Google is offering students stipends to write code for DUNE this summer.

We have put together a short list of [possible project ideas](https://users.dune-project.org/projects/gsoc2016/wiki/Project_ideas), we had in mind. This list is not exclusive, so if you have a good idea, feel free to discuss with us.
 To get yourself used to DUNE, have a look at our [junior jobs](https://users.dune-project.org/projects/gsoc2016/wiki/Junior_jobs). We ask every student to solve at least one junior job prior to his application.

Further information can be found on the official [GSoC 2016 page of DUNE](https://summerofcode.withgoogle.com/organizations/5666115405479936/) and our [according user wiki page](https://users.dune-project.org/projects/gsoc2016/wiki).
