+++
date = "2013-02-18"
title = "Dune 2.2.1beta1 Released"
+++

A first set of release candidates for the upcoming bugfix 2.2.1 release is now available. This includes all core modules. You can download the tarballs from our [download](/releases/) page. Please go and test, and report the problems that you encounter.
