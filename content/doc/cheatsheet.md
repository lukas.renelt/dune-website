+++
title = "Cheat Sheet"
[menu.main]
parent = "docs"
weight = 3
+++
### DUNE Cheat Sheet

Here is an (incomplete) yet compact
[DUNE Cheat Sheet](/pdf/dune-cheat-sheet.pdf).  It is meant for people who
more-or-less know Dune (e.g. from a course) but may need a little reminder and
don't want to sift their way through all the Details of the Doxygen
documentation.  Core only, no MPI, no adaptivity.

You can contribute [here](https://gitlab.dune-project.org/joe/dune-cheat-sheet).
